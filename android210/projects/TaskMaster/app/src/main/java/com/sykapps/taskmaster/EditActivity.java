package com.sykapps.taskmaster;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.sykapps.taskmaster.common.TaskState;
import com.sykapps.taskmaster.data.TasksDBHelper;
import com.sykapps.taskmaster.model.Task;


public class EditActivity extends ActionBarActivity implements View.OnClickListener, TextWatcher {

    private String taskId;
    private String taskName;
    private String taskDescription;
    private TaskState taskState;
    private EditText txtName;
    private EditText txtDescription;
    private Spinner taskStateSpinner;
    private Button buttonDelete;
    private Button buttonSave;
    private Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Intent intent = getIntent();
        taskId = String.valueOf(intent.getExtras().getInt("ID"));
        TasksDBHelper dbHelper = new TasksDBHelper(this);
        taskStateSpinner = (Spinner) findViewById(R.id.spinnerTaskState);
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, TaskState.getAllValuesToList());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        taskStateSpinner.setAdapter(dataAdapter);

        task = dbHelper.getRecordById(taskId);

        txtName = (EditText) findViewById(R.id.editTextTaskName);
        txtName.addTextChangedListener(this);
        txtDescription = (EditText) findViewById(R.id.editTextDescription);
        txtDescription.addTextChangedListener(this);
        txtName.setText(task.getTaskName());
        txtDescription.setText(task.getDescription());
        taskStateSpinner.setSelection(dataAdapter.getPosition(task.getState().toString()));

        buttonDelete = (Button) findViewById(R.id.buttonDelete);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonDelete.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        TasksDBHelper mDBHelper = new TasksDBHelper(this);
        Intent intent;
        switch (v.getId()) {
            case R.id.buttonDelete:
                mDBHelper.deleteRecord(taskId);
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonSave:
                if (this.task != null) {
                    task.setState(taskStateSpinner.getSelectedItem().toString());
                    mDBHelper.updateRecordById(this.task);
                    intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        //Update the local variables as the fields are updated
        if (txtDescription.getText().length() > 0) {
            task.setDescription(txtDescription.getText().toString());
        }
        if (txtName.getText().length() > 0) {
            task.setTaskName(txtName.getText().toString());
        }

    }
}
