package com.sykapps.taskmaster;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.sykapps.taskmaster.data.TasksDBHelper;
import com.sykapps.taskmaster.model.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener {

    private Date currentDate;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra("ID", taskList.get(position).getId());
        startActivity(intent);
    }

    private class TaskListArrayAdapter extends ArrayAdapter<Task> {

        private ArrayList<Task> tasks;

        public TaskListArrayAdapter(Context context, int resource, ArrayList<Task> tasks) {
            super(context, resource, tasks);
            this.tasks = tasks;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater =
                        (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.task_item, null);
            }

            Task task = tasks.get(position);

            if (task != null) {
                TextView txtViewTaskName = (TextView) view.findViewById(R.id.textViewTaskName);
                TextView txtViewTaskDescription = (TextView) view.
                        findViewById(R.id.textViewTaskDescription);
                TextView txtViewTaskState = (TextView) view.findViewById(R.id.textViewTaskState);

                if (task.getTaskName() != null) {
                    txtViewTaskName.setText(task.getTaskName());
                }

                if (task.getDescription() != null) {
                    txtViewTaskDescription.setText(task.getDescription());
                }

                if (task.getState() != null) {
                    txtViewTaskState.setText(task.getState().toString());
                }
            }
            return view;
        }
    }

    private TasksDBHelper mDbHelper;
    private ArrayList<Task> taskList;
    private ListView mListViewTasks;
    private TaskListArrayAdapter mTaskArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set the date instance for now.
        currentDate = new Date();

        setContentView(R.layout.activity_main);
        mDbHelper = new TasksDBHelper(this);
        taskList = mDbHelper.getTasks();
        if (taskList != null) {
            mListViewTasks = (ListView) findViewById(R.id.fragment_container_task_list);
            mTaskArrayAdapter = new TaskListArrayAdapter(this, R.id.taskItem, taskList);
            mListViewTasks.setAdapter(mTaskArrayAdapter);
            mListViewTasks.setOnItemClickListener(this);
            /**
             * This snippet is to include the sorting features
             * This has been disabled since date time is currently not working.
            if (taskList.size() > 1) {
                Spinner spinner = (Spinner) findViewById(R.id.spinnerSortBy);
                // Create an ArrayAdapter using the string array and a default spinner layout
                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                        R.array.sort_by, R.layout.spinner_item);
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // Apply the adapter to the spinner
                spinner.setAdapter(adapter);
            }
             */
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            mDbHelper = new TasksDBHelper(this);
            taskList = mDbHelper.getTasks();
            mListViewTasks = (ListView) findViewById(R.id.fragment_container_task_list);
            mTaskArrayAdapter = new TaskListArrayAdapter(this, R.id.taskItem, taskList);
            mListViewTasks.setAdapter(mTaskArrayAdapter);
            mListViewTasks.setOnItemClickListener(this);
            return true;
        }

        if (id == R.id.create_task) {
            Intent intent = new Intent();
            intent.setClass(this, TaskCreateActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Gets the date difference between the current date and the date of the task
     *
     * @param dueDate
     * @return
     */
    private String getDateDifference(Date dueDate) {
        if (currentDate == null) {
            currentDate = new Date();
        }
        long different = dueDate.getTime() - currentDate.getTime();

        System.out.println("startDate : " + currentDate);
        System.out.println("endDate : " + dueDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;
        String dateDiff = elapsedDays + "d " + elapsedHours + "h " + elapsedMinutes + "s";
        return dateDiff;
    }
}
