package com.sykapps.taskmaster;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.sykapps.taskmaster.data.TasksDBHelper;
import com.sykapps.taskmaster.model.Task;


/**
 * This activity is to create tasks
 */
public class TaskCreateActivity extends ActionBarActivity implements View.OnClickListener,
        TextWatcher, DatePickerFragment.OnFragmentInteractionListener,
        TimePickerFragment.OnFragmentInteractionListener {

    private String taskName;
    private String taskDescription;
    private Button btnSave;
    private EditText editTextTaskName;
    private EditText editTextDescription;
    //private Button btnAddDate;
    //private DatePicker datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_create);
        btnSave = (Button) findViewById(R.id.btnTaskCreate);
        btnSave.setOnClickListener(this);
        editTextTaskName = (EditText) findViewById(R.id.editTextTaskName);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        editTextTaskName.addTextChangedListener(this);
        editTextDescription.addTextChangedListener(this);

        /**btnAddDate = (Button) findViewById(R.id.btnAddDueDate);
        btnAddDate.setOnClickListener(this);**/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task_create, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTaskCreate:
                TasksDBHelper mDbHelper = new TasksDBHelper(this);
                mDbHelper.insertTaskItem(new Task(taskName, taskDescription));
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (editTextTaskName.getText().length() > 0 && editTextDescription.getText().length() > 0) {
            taskName = editTextTaskName.getText().toString();
            taskDescription = editTextDescription.getText().toString();
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
        }
    }

    /**
     * This method should show the Dialog Fragment for choosing Time
     * This is experimental currently and is failing
     * @param view
     */
    public void showTimePicker(View view) {
        DialogFragment timePickerFragment = new TimePickerFragment();
        timePickerFragment.show(getFragmentManager(),"timePicker");
    }

    /**
     * This method should show the Dialog Fragment for choosing Date
     * This is experimental currently and is failing
     * @param view
     */
    public void showDatePicker(View view) {
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}