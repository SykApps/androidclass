package com.sykapps.taskmaster.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Enumeration of task states which are allowed.
 */
public enum TaskState {
    NEW("New"),
    IN_PROGRESS("In Progress"),
    DELAYED("Delayed"),
    COMPLETE("Complete");

    private String state;

    TaskState(String state) {
        this.state = state;
    }

    /**
     * Gets the value associated with the selected task
     *
     * @return String task value.
     */
    @Override
    public String toString() {
        return this.state;
    }

    //Searches through the enums to return the proper task state by the string passed in.
    public static TaskState getTaskStateFromString(String state) {
        for (TaskState A : TaskState.values()) {
            if (A.toString().toLowerCase().equals(state.toLowerCase())) {
                return A;
            }
        }
        //Throw an error if the item is not present as it should not be being searched for
        throw new EnumConstantNotPresentException(TaskState.class, state);
    }

    public static List<String> getAllValuesToList() {
        List<String> list = new ArrayList<String>();
        for (TaskState A : TaskState.values()) {
            list.add(A.toString());
        }
        return list;
    }
}
