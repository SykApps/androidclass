package com.sykapps.taskmaster.data;

import android.provider.BaseColumns;

/**
 * Android Contract for Database
 */
public class AndroidContract implements BaseColumns {

    public static final String DATABASE_NAME = "tasks";

    /**
     * Define the Task table
     */
    public static final class Tasks {
        /**
         * Define table name
         */
        public static final String TABLE_NAME = "task";

        /*
         * Define table columns
         */
        public static final String ID = BaseColumns._ID;
        public static final String TASK_NAME = "task_name";
        public static final String DESCRIPTION = "description";
        public static final String TASK_STATE = "task_state";

        /*
         * Define Projections for Task
         */
        public static final String[] PROJECTION = new String[]{
            /*0*/AndroidContract.Tasks.ID,
            /*1*/AndroidContract.Tasks.TASK_NAME,
            /*2*/AndroidContract.Tasks.DESCRIPTION,
            /*3*/AndroidContract.Tasks.TASK_STATE
        };
    }
}
