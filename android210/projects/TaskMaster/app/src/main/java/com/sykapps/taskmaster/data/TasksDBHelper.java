package com.sykapps.taskmaster.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sykapps.taskmaster.common.TaskState;
import com.sykapps.taskmaster.model.Task;

import java.util.ArrayList;

/**
 * Tasks Database Helper
 */
public class TasksDBHelper extends SQLiteOpenHelper {
    private final static String LOG_TAG = TasksDBHelper.class.getSimpleName();

    private final static String DB_NAME = AndroidContract.DATABASE_NAME;

    private final static int DB_VERSION = 3;
    private final static String TASKS_TABLE_NAME = AndroidContract.Tasks.TABLE_NAME;
    private final static String TASKS_ID = AndroidContract.Tasks.ID;
    private final static String TASK_NAME = AndroidContract.Tasks.TASK_NAME;
    private final static String TASK_DESCRIPTION = AndroidContract.Tasks.DESCRIPTION;
    private final static String TASK_STATE = AndroidContract.Tasks.TASK_STATE;

    private final static String TASKS_TABLE_CREATE =
            "CREATE TABLE " + TASKS_TABLE_NAME + " (" +
                    TASKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    TASK_NAME + " TEXT NOT NULL, " +
                    TASK_DESCRIPTION + " TEXT NOT NULL, " +
                    TASK_STATE + " TEXT NOT NULL )";


    /**
     * Basic constructor for the TasksDBHelper
     *
     * @param context
     */
    public TasksDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TASKS_TABLE_CREATE);
        Log.i(LOG_TAG, "Creating tasks table with query " + TASKS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertTaskItem(Task task) {
        //Open the connection to the database as writable
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TASK_NAME, task.getTaskName());
        contentValues.put(TASK_DESCRIPTION, task.getDescription());
        contentValues.put(TASK_STATE, task.getState().toString());

        db.insert(TASKS_TABLE_NAME, null, contentValues);

        //Make sure to close the connection to the database
        db.close();
    }

    /**
     * Returns all the tasks in the database
     *
     * @return List of Task
     */
    public ArrayList<Task> getTasks() {
        //Open the database as read only
        SQLiteDatabase db = this.getReadableDatabase();

        //Create a new array of tasks
        ArrayList<Task> tasks = new ArrayList<Task>();

        //Create the cursor by executing the query
        Cursor cursor = db.query(TASKS_TABLE_NAME,
                AndroidContract.Tasks.PROJECTION,
                null, null, null, null, null);

        //Move the cursor to the first row and then start iterating through the rows
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Task task = cursorToTaskItem(cursor);
            tasks.add(task);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return tasks;
    }

    /**
     * Converts the cursor obect into a task
     *
     * @param cursor Cursor
     * @return Task
     */
    private Task cursorToTaskItem(Cursor cursor) {
        Task task = new Task(cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                TaskState.getTaskStateFromString(cursor.getString(cursor.getColumnIndexOrThrow(
                        AndroidContract.Tasks.TASK_STATE))));

        return task;
    }

    /**
     * Deletes a record by the specified taskId
     *
     * @param taskId
     */
    public void deleteRecord(String taskId) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TASKS_TABLE_NAME, TASKS_ID + "=?", new String[]{taskId});
        db.close();
    }

    public Task getRecordById(String id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TASKS_TABLE_NAME, AndroidContract.Tasks.PROJECTION,
                TASKS_ID + "=?", new String[]{id}, null, null, null, "1");
        cursor.moveToFirst();
        Task task = cursorToTaskItem(cursor);
        cursor.close();
        db.close();
        return task;
    }

    /**
     * Updates the specified record in the database
     *
     * @param task - Task
     */
    public void updateRecordById(Task task) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TASK_NAME, task.getTaskName());
        contentValues.put(TASK_DESCRIPTION, task.getDescription());
        contentValues.put(TASK_STATE, task.getState().toString());
        db.update(TASKS_TABLE_NAME, contentValues, TASKS_ID + "=?", new String[]{String.valueOf(
                task.getId())});
    }
}
