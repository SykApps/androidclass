package com.sykapps.taskmaster.model;

import com.sykapps.taskmaster.common.TaskState;

/**
 * Created by moizn on 3/8/15.
 */
public class Task {
    private int id;
    private String taskName;
    private String description;
    private TaskState state;

    //Primary Constructor

    /**
     * Initializes the object with all the specified parameters. This object should be created by
     * objects being pulled from the database.
     *
     * @param id          - databaseId
     * @param taskName    - String task name
     * @param description - String description
     * @param state       - TaskState state
     */
    public Task(int id, String taskName, String description, TaskState state) {
        this.id = id;
        this.taskName = taskName;
        this.description = description;
        this.state = state;
    }

    /**
     * Initializes the task into a new state with the specified task name and description
     *
     * @param taskName
     * @param description
     */
    public Task(String taskName, String description) {
        this.taskName = taskName;
        this.description = description;
        this.state = TaskState.NEW;
    }

    /**
     * Gets the item's id which is referenced in the database
     *
     * @return String id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Sets the task name of the task to the specified String
     *
     * @param taskName String
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * Gets the task name of this task
     *
     * @return String taskName
     */
    public String getTaskName() {
        return this.taskName;
    }

    /**
     * Sets the description of the task
     *
     * @param description String
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the description of this task
     *
     * @return String description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets the state of this task
     *
     * @param state TaskState
     */
    public void setState(TaskState state) {
        this.state = state;
    }

    public void setState(String state) {
        this.state = TaskState.getTaskStateFromString(state);
    }

    /**
     * Gets the state of this task
     *
     * @return TaskState
     */
    public TaskState getState() {
        return this.state;
    }
}
